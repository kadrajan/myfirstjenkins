package week3.day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TrainNames {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("https://erail.in");
		driver.findElementById("txtStationFrom").clear();
		
		driver.findElementById("txtStationFrom").sendKeys("MDU",Keys.TAB);
		
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("TVC",Keys.TAB);
		Thread.sleep(2000);
		
		WebElement myTable = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> selCol = myTable.findElements(By.tagName("tr"));
		System.out.println(selCol.size());
		
		for(int i=0; i<selCol.size()-1; i++) {
			
			List<WebElement> allCol = selCol.get(i).findElements(By.tagName("td"));
			String text = allCol.get(1).getText();
			System.out.println(text);
			
			
		}
		
		
		driver.close();

	}

}
