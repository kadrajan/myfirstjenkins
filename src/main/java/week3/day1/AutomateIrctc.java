package week3.day1;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AutomateIrctc {

	public static void main(String[] args) throws Exception {
		
		
		AutomateIrctc irctc = new AutomateIrctc();
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		driver.findElementById("userRegistrationForm:userName").sendKeys("iamback123");
		driver.findElementById("userRegistrationForm:password").sendKeys("Iamback123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Iamback123");
		
		
		Select sec = new Select(driver.findElementById("userRegistrationForm:securityQ"));
		sec.selectByValue("0");
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("doggy");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("First name");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("Middle name");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Last name");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();
		
		Select day = new Select(driver.findElementById("userRegistrationForm:dobDay"));
		day.selectByVisibleText("23");
		
		Select month = new Select(driver.findElement(By.id("userRegistrationForm:dobMonth")));
		month.selectByValue("03");
		
		Select year = new Select(driver.findElementById("userRegistrationForm:dateOfBirth"));
		year.selectByValue("1994");
		
		Select occ = new Select(driver.findElementById("userRegistrationForm:occupation"));
		occ.selectByValue("4");
		
		Select count = new Select(driver.findElementById("userRegistrationForm:countries"));
		count.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:email").sendKeys("iamback123@gmaiiil.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9876543210");
		
		
		Select nation = new Select(driver.findElementById("userRegistrationForm:nationalityId"));
		nation.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("Adddress1");
		
		driver.findElementById("userRegistrationForm:pincode").sendKeys("625009",Keys.TAB);
		
		/*Select city = new Select(driver.findElementById("userRegistrationForm:cityName"));
		city.selectByIndex(1);
		*/
		Thread.sleep(4000);
		irctc.selectdrop(driver.findElementById("userRegistrationForm:cityName"), 1);
		Thread.sleep(4000);
		irctc.selectdrop(driver.findElementById("userRegistrationForm:postofficeName"), 1);
		
		
		
		
		
		
		
		
		
		
		Thread.sleep(3000);
		//driver.close();
		
	}
	
	public void selectdrop(WebElement webElement, int Index) {
		
		Select sel = new Select(webElement);
		sel.selectByIndex(Index);
		
		
	}
	

}
