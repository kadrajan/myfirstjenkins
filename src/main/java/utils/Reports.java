package utils;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public abstract class Reports {
	
	public static ExtentReports extent;
	public String TcName, TcDesc, author, category;
	public ExtentHtmlReporter html;
	public static ExtentTest logger;
	
	//@BeforeSuite
	public void startReports() {
		html = new ExtentHtmlReporter("./Reports/result.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}
	
	//@BeforeMethod
	public void beforeMethod() {
		
		System.out.println(TcName+"   "+TcDesc);
		logger = extent.createTest(TcName,TcDesc);
		logger.assignAuthor(author);
		logger.assignCategory(category);
		System.out.println("Data Captured in Before Method");
		System.out.println(TcName+"   "+TcDesc);
				
	}
	
	public void reportStep(String status, String desc) {
		if (status.equalsIgnoreCase("pass")) {
			logger.log(Status.PASS, desc);			
		} else if (status.equalsIgnoreCase("fail")) {
			logger.log(Status.FAIL, desc);			
		}
	}
	
	
	//@AfterSuite
	public void endResult() {
		extent.flush();
		
	}
	
	
	
	
	
	

}
