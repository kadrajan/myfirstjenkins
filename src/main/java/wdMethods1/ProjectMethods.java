package wdMethods1;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import utils.ReadExcel;

public class ProjectMethods extends SeMethods{
	
	public String dataSheetName;
	@BeforeMethod
	public void login() {
		System.out.println("Data going to capture in Before Method");
		beforeMethod();
		System.out.println("hmm Before Method");
		startApp("chrome", "http://leaftaps.com/opentaps");
	}
	
	public void SelectDateMonthByJS(WebElement ele,String dateVal) {
		
		System.out.println("Js initialized");
		JavascriptExecutor js = (JavascriptExecutor)driver;
		System.out.println("js started");
		js.executeScript("arguments[0].setAttribute('value','"+dateVal+"');", ele);
		System.out.println("js job done");
		
		
		
	}
	
	public String getXpath(WebElement ele) {
	    String str = ele.toString();
	    String[] listString = null;
	    if(str.contains("xpath"))
	      listString = str.split("xpath:");
	    else if(str.contains("id"))
	      listString = str.split("id:");
	    String last = listString[1].trim();
	    return last.substring(0, last.length() - 1);
	}
	
	
	public int getCurrentdate() {
		// Get the current date
				Date date = new Date();
		// Get only the date (and not month, year, time etc)
				DateFormat sdf = new SimpleDateFormat("dd"); 
		// Get today's date
				String today = sdf.format(date);
				int dateToday = Integer.parseInt(today);
				return dateToday;
		
	}
	
	
	
	
	
	
	@BeforeTest
	public void beforeTest() {
		System.out.println("I am in BeforeTest in Project Methods Class");
	}
	@BeforeClass
	public void beforeClass() {
		System.out.println("@BeforeClass");
	}
	
	@AfterMethod
	public void closeApp() {
		
		System.out.println("Going to leave AfterMethod");
		closeBrowser();
	}
	@AfterClass
	public void afterClass() {
		System.out.println("@AfterClass");
	}
	@AfterTest
	public void afterTest() {
		System.out.println("@AfterTest");
	}
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	
	@BeforeSuite
	public void beforeSuite() {
		startReports();
	}
	
	
	@DataProvider(name="qa")
	public Object[][] getData() throws IOException{
		Object[][] data = ReadExcel.readExcel(dataSheetName);
		return data;
		
		//return DataInputProvider.getSheet(datasheetName);
	}
	
	

}
