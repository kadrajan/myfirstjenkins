package wdMethods1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LearnCalendar extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		System.out.println("Before test in Class");
		
		TcName = "TC001_LearnCal";
		TcDesc = "Learn Calendar";
		author = "karthik";
		category = "regression";
	}
	
	
	@Test
	public void LearnCal() throws InterruptedException {
		startApp("chrome", "https://www.goibibo.com/");
		
		WebElement ele = driver.findElementByXPath("//input[@class='form-control inputTxtLarge widgetCalenderTxt']");
		System.out.println(ele.getText());
		SelectDateMonthByJS(ele, "Thu, 1 Nov");
		
		Thread.sleep(45000);
	}
	
}
