package wdMethods1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import utils.Reports;

public class SeMethods extends Reports implements WdMethods{
	public int i = 1;
	public static RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				
				
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				/*Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("profile.default_content_setting_values.notifications", 2);
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("prefs", prefs);
				driver = new ChromeDriver(options);*/
				driver = new ChromeDriver();
				
				
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.manage().window().maximize();
			driver.get(url);
			
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
			reportStep("Pass", "The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			System.out.println("The Browser "+browser+" not Launched ");
			reportStep("Fail", "The Browser "+browser+" not Launched" );
		} finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": WebElement eleID = driver.findElementById(locValue);
					   reportStep("Pass", "Element is located with "+locValue+" using id Locator");
					   return eleID;
					  
			case "class": WebElement eleClass = driver.findElementByClassName(locValue);
			 			  reportStep("Pass", "Element is located with "+locValue+" using class Locator");
			 			  return eleClass;
			
			case "xpath": WebElement eleXpath = driver.findElementByXPath(locValue);
						  reportStep("Pass", "Element is located with "+locValue+" using XPath Locator");
			              return eleXpath;
			default: return null; 
			
			}
		} catch (NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
			reportStep("Fail", "Element cannot be located with "+locValue);
			return null;
		
		}catch(Exception e) {
			System.out.println("Locate element is failed");
			reportStep("Fail", "Element cannot be located with "+locValue);
			return null;
		}
		
		
	}

	@Override
	public WebElement locateElement(String locValue) {
		try {
			 WebElement eleID = driver.findElementById(locValue);
			 reportStep("Pass", "Element is loacted with "+locValue+" using ID locator");
			 return eleID;
		} catch (Exception e) {
			reportStep("Fail", "Element cannot be located with "+locValue);
			System.out.println("Element is not located");
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			System.out.println("The Data "+data+" is Entered Successfully");
			reportStep("Pass", "The data "+data+" is Entered Successfully");
			takeSnap();
		} catch (NoSuchElementException e) {
			System.out.println("The element could not be Located");
			reportStep("Fail", "The data "+data+" cannot be entered");
			
		}catch(Exception e) {
			System.out.println("The Data "+data+" is not Entered Successfully");
			reportStep("Fail", "The data "+data+" cannot be entered");
			
		}finally {takeSnap();}
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			reportStep("Pass", "The Element "+ele+" Clicked successfully");
			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (Exception e) {
			System.out.println("Click is not successful");
			reportStep("Fail", "The Element "+ele+" was not Clicked");
			e.printStackTrace();
		}
		takeSnap();
	}

	@Override
	public String getText(WebElement ele) {
		try {
			String text = ele.getText();
			System.out.println(text);
			return text;
		} catch (Exception e) {
			System.out.println("Element is not located");
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			System.out.println("The Dropdown Is Selected with "+value);
			reportStep("Pass", "The Dropdown Is selected with dropdown value"+value);
		} catch (Exception e) {
			System.out.println("The Dropdown is not selected with "+value);
			reportStep("Fail", "The Dropdown is not selected with dropdown value "+value);
			e.printStackTrace();
		}finally {
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);
			System.out.println("The dropdown is select with" +index+"index");
			reportStep("Pass", "The Dropdown is selected with index value "+index);
		} catch (Exception e) {
			System.out.println("The dropdown is not selected with" +index+"index");
			reportStep("Fail", "The Dropdown cannot be selected with index value "+index);
			e.printStackTrace();
		}finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		try {
			String title = driver.getTitle();
			
			if(title.equals(expectedTitle)) {
				System.out.println("Title is verified and it is same");
				reportStep("Pass", "Title is verified successfully which is as expected as "+expectedTitle);
				return true;
				
				
			}else {
				System.out.println("Title is not same");
				reportStep("Fail", "Title is not as expected.  The expected Title should be "+expectedTitle);
				return false;
				
			}
		} catch (Exception e) {
			System.out.println("Some thing wrong in verify title method..read exception message");
			reportStep("Fail", "Title is not as expected.  The expected Title should be "+expectedTitle);
			e.printStackTrace();
			return false;
		}
		
		
		
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String t1 = ele.getText();
			if(t1.equals(expectedText)) {
				System.out.println("WebElement has text "+ele.getText()+" "+"which is same as "+"expected text "+expectedText);		
				reportStep("Pass", "WebElement has text "+ele.getText()+" "+"which is same as "+"expected text "+expectedText);
			}else {
				System.out.println("WebElement has text "+ele.getText()+" "+"which is not same as "+"expected text "+expectedText);
				reportStep("Fail", "WebElement has text "+ele.getText()+" "+"which is not same as "+"expected text "+expectedText);
			}
		} catch (Exception e) {
			System.out.println("Element not available");
			reportStep("Fail", "WebElement has text "+ele.getText()+" "+"which is not same as "+"expected text "+expectedText);
			e.printStackTrace();
		}finally {
			takeSnap();
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String t1 = ele.getText();
			if(t1.contains(expectedText)) {
				System.out.println("WebElement with text "+ele.getText()+" "+"is same as "+"expected text "+expectedText+ "atleast partially");
				reportStep("Pass", "WebElement has text "+ele.getText()+" "+"matches "+"expected text "+expectedText);
			}else {
				System.out.println("WebElement with text "+ele.getText()+" "+"is not same as "+"expected text "+expectedText+ "atleast partially");
				reportStep("Fail", "WebElement has text "+ele.getText()+" "+"does not match with "+"expected text "+expectedText);
			}
		} catch (Exception e) {
			System.out.println("Element not available");
			reportStep("Fail", "WebElement has text "+ele.getText()+" "+"does not match with "+"expected text "+expectedText);
			e.printStackTrace();
		}finally {
			takeSnap();
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		
		try {
			switch (attribute) {
			case "id":
				String at2 = ele.getAttribute("id");
				if(at2.equals(value)) {
					System.out.println("The attribute id has the value "+value+" as expected");
					reportStep("Pass", "The attribute id has the value "+value+" as expected");
				}else {
					System.out.println("The attribute id does not have the value "+value);
					reportStep("Fail", "The attribute id does not have the value "+value);
				}
				
				break;
				
			case "class":
				String classVal = ele.getAttribute("class");
				if(classVal.equals(value)) {
					System.out.println("The attribute class has the value "+value+" as expected");
					reportStep("Pass", "The attribute class has the value "+value+" as expected");
				}else if (classVal.contains(value)){
					System.out.println("The attribute class contains  the value "+value);
					reportStep("Pass", "The attribute class contains  the value "+value);
				}else {
					System.out.println("The attribute class does not have the value "+value);
					reportStep("Fail", "The attribute class does not have the value "+value);
					
				}
				
				break;
			case "name":
				String at3 = ele.getAttribute("name");
				if(at3.equals(value)) {
					System.out.println("The attribute name has the value "+value+" as expected");
					reportStep("Pass", "The attribute name has the value "+value+" as expected");
				}else {
					System.out.println("The attribute name does not have the value "+value);
					reportStep("Fail", "The attribute name does not have the value "+value);
				}
				break;
			
			default:
				System.out.println("The attribute tag should be typed only >> id or name");
				break;
			}
		} catch (Exception e) {
			System.out.println("Element not available or some other issue");
			reportStep("Fail", "Element not available with the given tag having the value "+value);
			
			e.printStackTrace();
		}finally {
			takeSnap();
		}
		

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		switch (attribute) {
		case "id":
			String at2 = ele.getAttribute("id");
			if(at2.contains(value)) {
				System.out.println("The attribute id contains the value "+value+" as expected");
				reportStep("Pass", "The attribute id contians the value "+value+" as expected");
			}else {
				System.out.println("The attribute id does not contain the value "+value);
				reportStep("Fail", "The attribute id does not contian the value "+value+" as expected");
			}
			
			break;
		case "name":
			String at3 = ele.getAttribute("name");
			if(at3.contains(value)) {
				System.out.println("The attribute name contians the value "+value+" as expected");
				reportStep("Pass", "The attribute name contians the value "+value+" as expected");
			}else {
				System.out.println("The attribute name does not contains the value "+value);
				reportStep("Fail", "The attribute name does not contian the value "+value+" as expected");
			}
			break;
		
		default:
			System.out.println("Enter a valid attribute >> id or name");
			reportStep("Fail", "Element not available with the given tag having the value "+value);
			break;
		}

	}

	@Override
	public void verifySelected(WebElement ele) {
		
		try {
			if(ele.isSelected()) {
				System.out.println("verified that the elemennt"+ele+" is selected");
				reportStep("Pass", "Verified that the element "+ele+ "is selected");
			}else {
				System.out.println("verified that the elemennt"+ele+" is not selected");
				reportStep("Fail", "Verified that the element "+ele+ "is not selected");
			}
			takeSnap();
		} catch (Exception e) {
			reportStep("Fail", "Element is not available ");
			
			e.printStackTrace();
		}
		

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
			if(ele.isDisplayed()) {
				System.out.println("verified that the elemennt"+ele+" is displayed");
				reportStep("Pass", "Verified that the element "+ele+ "is displayed");
			}else {
				System.out.println("verified that the elemennt"+ele+" is not displayed");
				reportStep("Fail", "Verified that the element "+ele+ "is not displayed");
			}
			takeSnap();
		} catch (Exception e) {
			reportStep("Fail", "Element is not verified to be displayed");
			e.printStackTrace();
		}

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		driver.switchTo().window(listOfWindow.get(index));
		System.out.println("The Window is Switched ");
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);
		System.out.println("Frame switched to webElement location "+ele);
	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		takeSnap();
	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
		takeSnap();

	}

	@Override
	public String getAlertText() {
		String text = driver.switchTo().alert().getText();
		return text;
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
		System.out.println("Window closed_test");

	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
		System.out.println("All windows closed");

	}

}
