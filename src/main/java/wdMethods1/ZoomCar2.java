package wdMethods1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ZoomCar2 extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		System.out.println("I am in Before test in Zoom Car Class");
		
		TcName = "TC001_ZoomCarRun";
		TcDesc = "Create A new Lead";
		author = "karthik";
		category = "regression";
	}
	
	
	@Test
	public void zoomCar() {
		startApp("chrome", "https://www.zoomcar.com");
		click(locateElement("xpath", "//a[contains(text(),'Start your wonderful journey')]"));
		click(locateElement("xpath", "//div[contains(text(),'Porur')]"));
		click(locateElement("xpath", "//button[contains(text(),'Next')]"));
		int tmrwDate = getCurrentdate()+1;
		//div[@class='day'][contains(text(),'"+tmrwDate+"')] 
		click(locateElement("xpath","//div[@class='day'][contains(text(),'"+tmrwDate+"')] "));
		click(locateElement("xpath", "//button[contains(text(),'Next')]"));
		//String text = getText(locateElement("xpath", "//div[@class='day picked ']//div"));
		//System.out.println("The text is: "+text);
		
		WebElement eleDate = locateElement("xpath", "//div[contains(text(),'"+tmrwDate+"')] ");
		//WebElement eleDate = locateElement("xpath", "//div[contains(text(),'16')] ");
		verifyExactAttribute(eleDate, "class", "day picked");
	
		click(locateElement("xpath", "//button[contains(text(),'Done')]"));
		
		List<WebElement> allPrices = driver.findElementsByXPath("//div[@class='price']");
		List<String> allGoodPrices = new ArrayList<>();
		
		for (WebElement eachPrice : allPrices) {
			System.out.println(eachPrice.getText());
			eachPrice.getText();
			
			allGoodPrices.add(eachPrice.getText().replaceAll("[^0-9]", "").trim());
			
		}
		
		System.out.println(allGoodPrices);
		
		Collections.sort(allGoodPrices);
		
		
		String HighestAmountCar = allGoodPrices.get(allGoodPrices.size()-1);
		System.out.println("HighestAmount of all the cars is "+HighestAmountCar);
		
		String bigCar = driver.findElementByXPath("//div[contains(text(),'"+HighestAmountCar+"')]/../../preceding-sibling::div[1]/h3").getText();
		System.out.println("Highest amount of the car is "+bigCar);
		
			
			
		}

	
	}
	


