package wdMethods1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class FacebookTestLeaf extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		System.out.println("Before test in Class");
		
		TcName = "TC001_TestLeafCheck";
		TcDesc = "TestLeafCheck";
		author = "karthik";
		category = "regression";
	}
	
	@Test
	public void fb() throws InterruptedException {
		
		//click(locateElement("id", ""));
		startApp("chrome", "https://www.facebook.com/");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		type(locateElement("email"), "karthik.deepan15@gmail.com");
		type(locateElement("pass"), "karthik101");
		click(locateElement("xpath", "//input[@value='Log In']"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@data-testid='search_input']")));
		type(locateElement("xpath", "//input[@data-testid='search_input']"), "TestLeaf");
		//click(locateElement("//button[@data-testid='facebar_search_button']/i"));
		driver.findElementByXPath("//button[@data-testid='facebar_search_button']/i").click();
		reportStep("pass", "Clicked on Search button");
		
		//getText(locateElement("xpath", "//button[contains(@class,'Like') and @tabindex='0']"));
		
		
		//Thread.sleep(5000);
		
		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class,'Like')]")));
		int count = 0;
		do {
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//button[contains(@class,'Like')]")));		
		
		List<WebElement> allEle = driver.findElementsByXPath("//button[contains(@class,'Like')]");
		
		for (WebElement eachEle : allEle) {
		//	WebElement eachEle;
			System.out.println(eachEle.getText());
			
			count = allEle.size();
			
		}
		System.out.println("=======================================================");
		}while(count< 5);
		
		
		
		
		
		
		
		
		
	
		
			
	}
	

}
