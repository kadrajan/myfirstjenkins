package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods1.ProjectMethods;

public class TC001_LoginLogout extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		
		TcName = "Login Logout";
		TcDesc = "Login into Leaftap";
		category = "smoke";
		author = "kgr";
		
		
		dataSheetName = "TC001";
		
		
	}
	
	
	@Test(dataProvider = "qa")
	public void loginLogout(String uName, String password) {
		
		/*LoginPage lp = new LoginPage();
		lp.enterUserName(uName);*/
		new LoginPage()
		.enterUserName(uName)
		.enterPassword(password)
		.clickLogin();
		
	}
	
	
	
	

}
