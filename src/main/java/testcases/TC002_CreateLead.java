package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods1.ProjectMethods;


public class TC002_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		TcName = "TC002_Create Lead";
		TcDesc = "Verify a lead can be created successfully";
		category = "smoke";
		author = "kgr";
		
		
		dataSheetName = "TC002";
		
	}
	
	
	@Test(dataProvider = "qa")
	public void createLead(String uName, String password, String compName, String fname, String lname) {
		new LoginPage().enterUserName(uName).enterPassword(password).clickLogin().clickEleCRMLink().clickLeads().clickCrLead().enterCompName(compName).enterFName(fname).enterLName(lname).clickCreateLead();
	
	}
	
	

}
