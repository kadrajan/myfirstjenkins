package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverCloseIRctc {

	    public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.get("https://www.irctc.co.in/nget/train-search");
		
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		driver.findElementByLinkText("Contact Us").click();
		
		Set<String> allWin = driver.getWindowHandles();
		List<String> allWindows = new ArrayList<>();
		allWindows.addAll(allWin);
		
		driver.switchTo().window(allWindows.get(1));
		
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snaps/img1.jpg");
		FileUtils.copyFile(src, dest);
		
		//delete all win except current windows
		String currwin = driver.getWindowHandle();
		
		
		
		for (String s : driver.getWindowHandles()) {
			
			if(!s.equals(currwin)) {
				driver.switchTo().window(s).close();
			}
			
		}
		
		
		
		
		

	}

}
