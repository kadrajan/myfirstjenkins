package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlertAndWindows {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/crmsfa/control/logout");
		driver.findElementByName("USERNAME").sendKeys("DemoSalesManager");;
		driver.findElementByName("PASSWORD").sendKeys("crmsfa");
		driver.findElementByClassName("loginButton").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a/img").click();
		
		Set<String> allwin = driver.getWindowHandles();
		List<String> allWin = new ArrayList<>();
		allWin.addAll(allwin);
		driver.switchTo().window(allWin.get(1));
		
		driver.findElementByName("firstName").sendKeys("G");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//a[@class='linktext']").click();
		
		driver.switchTo().window(allWin.get(0));
		
		driver.findElementByXPath("//table[@id='widget_ComboBox_partyIdTo']/following-sibling::a/img").click();
		
		Set<String> allwin1 = driver.getWindowHandles();
		List<String> allWin1 = new ArrayList<>();
		allWin1.addAll(allwin1);
		driver.switchTo().window(allWin1.get(1));
		
		
		driver.findElementByName("firstName").sendKeys("H");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//a[@class='linktext']").click();
		driver.switchTo().window(allWin1.get(0));
		
		driver.findElementByLinkText("Merge").click();
		
		driver.switchTo().alert().accept();
		
		

	}

}
