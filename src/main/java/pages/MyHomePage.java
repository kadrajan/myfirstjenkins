package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods1.ProjectMethods;



public class MyHomePage extends ProjectMethods {
	
	
	
	
	public MyHomePage() {
		PageFactory.initElements(driver, this);
	}
	
	
	//@FindBy(how = How.XPATH, using = "//a[text()='Leads']") WebElement eleLeads;
	

	
	public MyLeads clickLeads() {
		
		WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
		
		click(eleLeads);
		return new MyLeads();
		
		
		
	}

}
