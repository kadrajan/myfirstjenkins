package myPackage;

import java.util.Scanner;

//prog to find Prime number
public class PrimeNumber {
	
	public static void main(String[] args) {
		
		System.out.println("Enter a number");
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		int s=num/2;
		
		
		boolean flag = false;
		if(num==0||num==1) {
			System.out.println(num+" is not a prime number");
		}
		for(int i=2;i<=s;i++ ) {
			if(num%i==0) {
				
				flag=true;
				break;
			}
			
		}
		if(flag) {
			System.out.println(num+" is not a prime number");
		}else 	System.out.println(num+" is a prime number");	
		
	}

}
