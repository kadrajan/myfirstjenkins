package myPackage;

public class MobilePhone {
	
	double remainingBalance = 9;
		
	public void sendSMS(long mobileNumber, String SMS) 
		{
				
			System.out.println("The SMS "+SMS+" is successfully sent to the mobile number "+mobileNumber);
			remainingBalance = remainingBalance - 1;
			System.out.println("Your Remaining Balance is "+remainingBalance);
			
		}
	
	public void callContact(long mobileNumber)
		{	
			System.out.println("Calling successful");
			remainingBalance = remainingBalance - 2;
			System.out.println("Your Remaining Balance is "+remainingBalance);
		
		}

}
