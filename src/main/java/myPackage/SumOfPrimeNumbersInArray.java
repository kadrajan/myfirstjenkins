package myPackage;

import java.util.Scanner;

public class SumOfPrimeNumbersInArray {

	public static void main(String[] args) {
		System.out.println("Enter how many numbers");
		Scanner scan = new Scanner(System.in);
		int size = scan.nextInt();
		
		int[] numbers = new int[size];
		System.out.println("Enter the numbers");
		for(int i=0;i<size;i++) 
		{
			numbers[i]= scan.nextInt();
		}
		
		int sum = 0;
		CheckPrimeOrNot check = new CheckPrimeOrNot();
		
		for(int i=0; i<size;i++)
		{
			sum = sum + check.checkPrime(numbers[i]);
			
		}
		
		System.out.println("Sum of Prime numbers >>>>>"+sum);

	}

}
