package myPackage;

public class LearnConstructor {
	
	int num;
	
	public static void main(String[] args) {
		
		
		LearnConstructor lb = new LearnConstructor();
		//no need to call LearnConstructor Constructor, constructor will  be called by itself while creating object of the class
		lb.function1();
		
	}
	
	
	public LearnConstructor() {
		System.out.println("i am in constructor");
		
	}
	
	public void function1() {
		System.out.println("I am in fuction");
		System.out.println(num);
	}

}
