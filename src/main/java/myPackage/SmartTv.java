package myPackage;

public class SmartTv {
	
	public String brand = "SONY";
	public int warranty = 5;
	
	
	public void switchChannel(int chnum) {
		
		System.out.println("Switched to channel number  "+chnum);
		
	}
	
	public void setVolume(int volume) {
		
		System.out.println("Volume set to "+volume);
		
	}
	
	

}
