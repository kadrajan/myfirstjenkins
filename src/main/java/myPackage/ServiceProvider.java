package myPackage;

public class ServiceProvider {
	
	public void methodUsingSwitch(int number) 
		{
		
		switch (number) 
			{
			case 944:
				System.out.println("BSNL");
				break;
			case 900:
				System.out.println("AIRTEL");
				break;
				
			case 630:
				System.out.println("JIO");
				break;
				
			default:
				System.out.println("Invalid Input");
				break;
			}
		
		}
	public void methodUsingIf(int number)
		{
		if(number == 944)	System.out.println("BSNL");
		else if(number == 900)	System.out.println("AIRTEL");
		else if(number == 630)	System.out.println("JIO");
		else System.out.println("Invalid Input");
			
		}

}
