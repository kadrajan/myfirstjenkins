package myPackage;

import java.util.Scanner;

public class SumOfOddDigits {

	public static void main(String[] args) {
		System.out.println("Enter a number");
		Scanner scan = new Scanner(System.in);
		Integer num = scan.nextInt();
		
		String str = num.toString();
		//System.out.println("RIGHT HERE"+str.valueOf(str));;
		
		char[] chAr = str.toCharArray();
		
		int sum = 0;
		for(int i=0;i<chAr.length;i++){
		
		int a = Integer.parseInt(String.valueOf(chAr[i]));
		
		if(a%2==1) {
			sum = sum + a;
		}
		
		}
		
		System.out.println(sum);
		
		
		
	}

}
