package myPackage;

import java.util.Scanner;

public class LearnArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array:");
		int arrSize = sc.nextInt();
		int[] realArray = new int[arrSize];
		for(int i=0;i<arrSize;i++) {
			System.out.println("Enter the value"+(i+1)+":");
			realArray[i]=sc.nextInt();
		}
		for(int eachArr:realArray) {
			System.out.println("Value of Array is:"+eachArr);
		}
		int arrCount = 0;
		for(int eachArr:realArray) {
			if(eachArr%2==1) {
				arrCount = arrCount+eachArr;
				
			}
		}
		System.out.println("Sum of odd numbers in the array is:"+arrCount);
		sc.close();
	}

}
