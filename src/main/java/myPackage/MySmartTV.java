package myPackage;

public class MySmartTV {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SmartTv test = new SmartTv();
		int warranty = test.warranty;
		System.out.println("Warranty of the TV "+warranty);
		String brand = test.brand;
		System.out.println("Brand of the TV "+brand);
		//Methods
		test.switchChannel(123);
		test.setVolume(25);
	}

}
