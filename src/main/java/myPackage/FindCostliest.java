package myPackage;

import java.util.Scanner;

public class FindCostliest {

	public static void main(String[] args) {
		
		System.out.println("Enter the prices of two mobile phones");
		Scanner scan = new Scanner(System.in);
		double mob1 = scan.nextDouble();
		double mob2 = scan.nextDouble();
		
		if(mob1>mob2) {
			
			System.out.println("The mobile phone with price  "+mob1+" is COSTLIEST");
			System.out.println("The mobile phone with price  "+mob2+" is CHEAPEST");
		}
		else
		{
			System.out.println("The mobile phone with price  "+mob2+" is COSTLIEST");
			System.out.println("The mobile phone with price  "+mob1+" is CHEAPEST");
		}

	}

}
