package week2.day1;

public class MainMethod {

	public static void main(String[] args) {
		
		Television tv = new Television();
		tv.changeChannelNo();
		
		
		
		SmartTelevision smt = new SmartTelevision();
	 	smt.changeChannelNo();
		smt.enableChromeCase();
		
		
		
		Entertainment ent = new Television(); // OR Entertainment ent = new SmartTelevision();
		
		
		ent.enableMute();
		//In here only the abstract methods that are implemented by Television class will only be shown
		

	}

}
