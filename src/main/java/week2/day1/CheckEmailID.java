package week2.day1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckEmailID {

	public static void main(String[] args) {
	
		
		// String to be scanned to find the pattern.
	      String line = "karthik.deepan23@gmail.com";
	     // String n = "[a-z0-9]{15}";
	      
	      String pattern = "[a-z0-9\\.]{"+line.length()+"}";
	      System.out.println(pattern);
	      
	    		  
	      // Create a Pattern object
	      Pattern r = Pattern.compile(pattern);

	      // Now create matcher object.
	      Matcher m = r.matcher(line);
	      
	      System.out.println(m.matches());
	      
	   }
		
		

	}


