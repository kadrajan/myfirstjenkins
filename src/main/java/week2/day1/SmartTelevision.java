package week2.day1;

public class SmartTelevision extends Television {
	
	public void enableChromeCase() {
		System.out.println("Chrome case enabled");
	}
	
	public void playGames() {
		System.out.println("Playing games");
	
	}
	
	public void installApps() {
		System.out.println("Installing apps");
		
	}
	

}
