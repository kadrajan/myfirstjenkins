package week2.day2;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

//Add list of mobiles with price and find the max priced mobile


public class FindMaxValueFromListOfMobiles {

	public static void main(String[] args) 
	
	{
		Map<String,Integer> mobiles= new HashMap<>();
		mobiles.put("One Plus", 50000);
		mobiles.put("Apple", 6000);
		mobiles.put("Redmi", 50000);
		mobiles.put("Samsung", 9000);
		int max=0;
		for(Integer current: mobiles.values())
		{
			if(current>max) 
			{
				max=current;
				
			}
			
		}
		
		
		for(Entry <String,Integer> eachMobile : mobiles.entrySet()) {
			
			if(max == eachMobile.getValue()) {
				System.out.println(eachMobile.getKey()+"------>"+eachMobile.getValue());
			}
			
		}
		
		

	}

}
