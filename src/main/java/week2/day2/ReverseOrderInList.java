package week2.day2;

import java.util.ArrayList;
import java.util.List;

public class ReverseOrderInList {

	public static void main(String[] args) {
		// add few numbers in list & print in reverse order without using collections.sort
		// eg [1,2,3] o/p [3,2,1]
		
		List<Integer> numbers = new ArrayList<>();
		numbers.add(4);
		numbers.add(7);
		numbers.add(2);
		
		for(int i = numbers.size()-1 ; i>=0 ; i-- )
		{
		//System.out.println("I am in loop");
		System.out.print(numbers.get(i));
			
		}
		
		
		
		
		

	}

}
