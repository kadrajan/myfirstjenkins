package week2.day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ShowRecordsInOddPositions {

	public static void main(String[] args) {
		// Create a set object and add some list of numers and display records in odd positions

		Set<Integer> numbers = new HashSet<>();
		numbers.add(1);
		numbers.add(5);
		numbers.add(7);
		numbers.add(8);
		numbers.add(6);
		List<Integer> numbers_odd = new ArrayList<>();
		
		List<Integer> numbers_list = new ArrayList<>();
		numbers_list.addAll(numbers);
		
		System.out.println(numbers_list);
		
		for(int i=0; i<numbers_list.size(); i++) {
			if(i%2==0) 
			{
				
				numbers_odd.add(numbers_list.get(i));
											
			}
					
		}
		
		System.out.println(numbers_odd);
		
		
		
	}

	

}
